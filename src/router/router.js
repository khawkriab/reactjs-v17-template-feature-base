import React from "react";
import Swal from "sweetalert2";
import { Switch, Route, BrowserRouter, useHistory } from "react-router-dom";
import { Navbar, Sidebar } from "components/router";

// import pages
import Home from "pages/home";
import Template from "pages/template";
import { NotFound } from "pages/other";


function PrivateRoute(props) {
    let history = useHistory()
    const profile = JSON.parse(window.localStorage.getItem('_profile_'))
    if (!profile)
        Swal.fire({
            html:
                '<div>' +
                '<div class="swal2-title text-warm-lightseagreen"><i class="fas fa-exclamation-circle"></i> โปรดล็อกอิน</div>' +
                '<div>กรุณาล็อกอินเข้าสู่ระบบใหม่อีกครั้ง</div>' +
                '</div>',
            confirmButtonColor: '#00acaa',
            confirmButtonText: 'ตกลง',
        }).then(() =>
            // window.location.assign('/user/line')
            history.push('/home')
        )

    return (profile)
        ? <Route {...props} />
        : null
}

export default function Router() {

    return (
        <BrowserRouter>
            <div className="router">
                <Sidebar />
                <Navbar />
                <Switch>
                    <Route path="/" exact render={(props) => <Home {...props} />} />
                    {/* home */}
                    <Route path="/home" render={(props) => <Home {...props} />} />
                    <PrivateRoute path="/home" render={(props) => <Home {...props} />} />
                    <Route path="/template" render={(props) => <Template {...props} />} />

                    <Route path="*" render={(props) => <NotFound {...props} />} />

                </Switch>
            </div>
        </BrowserRouter>
    );
}
